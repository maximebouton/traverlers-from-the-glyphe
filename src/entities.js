

// # Entities


// ## Variables

const entities = []
const dead_entities = []

const attr = ["pv","at","de","as","ds"]

const types = {
  "brave" : {
    "pv" : 15,
    "at" : 2,
    "de" : 2,
    "as" : 1,
    "ds" : 1
  },
  "brutal" : {
    "pv" : 10,
    "at" : 3,
    "de" : 1,
    "as" : 2,
    "ds" : 1
  },
  "clever" : {
    "pv" : 5,
    "at" : 2,
    "de" : 3,
    "as" : 1,
    "ds" : 2
  },
  "mystic" : {
    "pv" : 5,
    "at" : 2,
    "de" : 1,
    "as" : 3,
    "ds" : 2
  },
  "devoted" : {
    "pv" : 10,
    "at" : 1,
    "de" : 2,
    "as" : 1,
    "ds" : 3
  }
}

const castes = {
  "blade" : {
      "blade"   :   2,
      "muscle"  :   1,
      "dice"    :   2,
      "veile"   :   1,
      "jar"     :   1,
      "spice"   :   2,
      "stone"   :   1,
      "card"    :  -1
  },
  "muscle" : {
      "blade"   :   2,
      "muscle"  :   1,
      "dice"    :   2,
      "veile"   :   1,
      "jar"     :   1,
      "spice"   :   2,
      "stone"   :   1,
      "card"    :  -1
  },
  "dice"   : {
      "blade"   :  -1,
      "muscle"  :  -1,
      "dice"    :  -1,
      "veile"   :  -1,
      "jar"     :  -1,
      "spice"   :  -1,
      "stone"   :  -1,
      "card"    :  -1
  },
  "veile"  : {
      "blade"   :   1,
      "muscle"  :   2,
      "dice"    :   2,
      "veile"   : 0.5,
      "jar"     : 0.5,
      "spice"   :   2,
      "stone"   : 0.5,
      "card"    :  -1 
  },
  "jar"    : {
      "blade"   : 0.5,
      "muscle"  :   1,
      "dice"    :   1,
      "veile"   :   1,
      "jar"     :   1,
      "spice"   :   1,
      "stone"   :   1,
      "card"    :  -1 
  },
  "spice"  : {
      "blade"   :   2,
      "muscle"  :   1,
      "dice"    :   2,
      "veile"   :   1,
      "jar"     :   1,
      "spice"   :   2,
      "stone"   :   1,
      "card"    :  -1 
  },
  "stone"  : {
      "blade"   : 0.5,
      "muscle"  :   4,
      "dice"    :   1,
      "veile"   : 0.5,
      "jar"     : 0.5,
      "spice"   :   0,
      "stone"   : 0.5,
      "card"    :  -1 
  },
  "card"   : {
      "blade"   : 0.5,
      "muscle"  :   1,
      "dice"    :   0,
      "veile"   : 0.5,
      "jar"     : 0.5,
      "spice"   :   4,
      "stone"   : 0.5,
      "card"    :  -1 
  }
}


// ## Related functions

function list_castes(){
  return Object.keys(castes)
}

function list_types(){
  return Object.keys(types)
}


// ## Related objects


// ### Cards

const Card = function(){
  this.type = ["bonus","malus","attack","special","block"].rdm()
  if ( this.type == "bonus" || this.type == "malus" ){
    this.attr = attr.rdm()
  }
}


// ### Entities

const Entity = function(name,type,caste,level){
  this.id
  this.name = name
  this.type = type
  this.caste = caste
  this.deck = []
  for ( var i = 0 ; i < 3 ; i ++ ) {
    this.deck.push(new Card())
  }
  this.hand = []
  this.discard = []
  this.combat_action = undefined
  this.combat_target = undefined
  this.team = undefined
  this.xp = 0
  this.lvl = 0
  this.pv = 0
  this.at = 0
  this.de = 0
  this.as = 0
  this.ds = 0
  this.wounds = []
  this.dimmers = {
    "pv" : 0,
    "at" : 0,
    "de" : 0,
    "as" : 0,
    "ds" : 0 
  }
  for ( var i = 0 ; i < level ; i++ ){
    this.xp += parseInt(Math.pow(this.lvl+1,this.lvl/10+1)-1)
    let leveling = this.level_up()
  }
  entities.push(this)
}

Entity.prototype.level_up = function(){
  let leveling = {} 
  for ( var i in types[this.type] ) {
    leveling[i] = types[this.type][i] 
                + (Math.random() > 0.5 ? parseInt(this[i]/10)+1 : 0)
    this[i] += leveling[i]
  }
  this.lvl++
  return [this.lvl,leveling]
}


// #### Combat related functions

Entity.prototype.bonus = function(attr,entity){
  var multiplier = castes[this.caste][entity.caste]
  if ( multiplier < 0 ) multiplier = [0,0.5,1,2,4].rdm()
  let bonus = ( this.as + this.dimmers["as"] ) * multiplier > 0 
            ? ( this.as + this.dimmers["as"] ) * multiplier : 0
  entity.dimmers[attr] += bonus
  return bonus
}

Entity.prototype.malus = function(attr,entity){
  var multiplier = castes[this.caste][entity.caste]
  if ( multiplier < 0 ) multiplier = [0,0.5,1,2,4].rdm()
  let malus = ( this.as + this.dimmers["as"] ) * multiplier 
            - entity.ds + entity.dimmers["ds"] > 0 
            ? ( this.as + this.dimmers["as"] ) * multiplier 
            - entity.ds + entity.dimmers["ds"] 
            : 0
  entity.dimmers[attr] -= malus
  return malus
}

Entity.prototype.attack = function(entity){
  let dmg = this.at + this.dimmers["at"] > 0 
          ? this.at + this.dimmers["at"]
          : 0
  entity.wounds.push(["normal",dmg])
  return dmg
}

Entity.prototype.special = function(entity){
  var multiplier = castes[this.caste][entity.caste]
  if ( multiplier < 0 ) multiplier = [0,0.5,1,2,4].rdm()
  let dmg = ( this.as + this.dimmers["as"] ) * multiplier > 0 
          ? ( this.as + this.dimmers["as"] ) * multiplier 
          : 0
  entity.wounds.push(["special",dmg])
  return dmg
}

Entity.prototype.hurt = function(){
  while ( this.wounds.length ) {
    var dmg
    if ( this.wounds[0][0] == "normal" ) {
      dmg = this.wounds[0][1] - ( this.de + this.dimmers["de"] ) > 0 
          ? this.wounds[0][1] - ( this.de + this.dimmers["de"] )
          : 0
    } else {
      dmg = this.wounds[0][1] - ( this.ds + this.dimmers["ds"] ) > 0 
          ? this.wounds[0][1] - ( this.ds + this.dimmers["ds"] )
          : 0
    }
    this.pv -= dmg
    console.log(this.name,"take",dmg,"damages")
    this.wounds.remove(this.wounds[0])
  }
}

Entity.prototype.draw = function(){
  while ( this.hand.length < 3 ) {
    if ( !this.deck.length ) {
      while ( this.discard.length ) {
        if ( Math.random < 0.5 ) {
          if ( Math.random < 0.5 ) {
            this.deck.push(this.discard.pop())
          } else {
            this.deck.unshift(this.discard.pop())
          }
        } else {
          if ( Math.random < 0.5 ) {
            this.deck.push(this.discard.shift())
          } else {
            this.deck.unshift(this.discard.shift())
          }
        }
      }
    }
    this.hand.push(this.deck.pop())
  }
}



// ## Teams

var teams = []

const Team = function(name,entities,ia){
  this.name = name
  this.entities = entities
  this.display = 0
  for ( var i = 0 ; i < this.entities.length ; i++ ) {
    this.entities[i].team = this
    this.entities[i].id = i
  }
  this.ia = ia
}

