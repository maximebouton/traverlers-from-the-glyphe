const assets_to_load = [
  ['src/assets/missing.txt','missing'],
  ['src/assets/brave_blade.txt','brave_blade'],
  ['src/assets/brave_dice.txt','brave_dice'],
  ['src/assets/brutal_dice.txt','brutal_dice'],
  ['src/assets/brutal_muscle.txt','brutal_muscle'],
  ['src/assets/brutal_veile.txt','brutal_veile'],
  ['src/assets/clever_dice.txt','clever_dice'],
  ['src/assets/clever_spice.txt','clever_spice'],
  ['src/assets/devoted_muscle.txt','devoted_muscle'],
  ['src/assets/devoted_spice.txt','devoted_spice'],
  ['src/assets/mystic_spice.txt','mystic_spice'],
]

function read(url, callback) {
    var xhr = new XMLHttpRequest()
    xhr.open("GET", url, true)
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            // defensive check
            if (typeof callback === "function") {
                // apply() sets the meaning of "this" in the callback
                callback.apply(xhr)
            }
        }
    }
    xhr.send()
}

function n(c,n){
  var str = ''
  for ( var i = 0 ; i < n ; i++ ){
    str += c
  }
  return str
}

function col(arr){
  
  let col = create('pre')
  
  for ( var i = 0 ; i < arr.length ; i++ ) {
    let brick = create('span')
    brick.innerText = arr[i][0]
    brick.style.color = arr[i][1]
    col.appendChild(brick)
  }
  
  return col
}

const card_colors = {
  'special':'#ff0',
  'attack':'#f00',
  'block':'#80f',
  'at':'#f00',
  'de':'#08f',
  'as':'#ff0',
  'ds':'#a0a',
  'pv':'#0f0',
}

function hand_row(team){

  let content = []
  
  for ( var i = 0 ; i < team.entities[team.display].hand.length ; i ++ ) {
    let entity = team.entities[team.display]
    let card = entity.hand[i]
    if ( entity.combat_action == undefined ) {
      
      keys[i] = function(key){
        this[0].combat_action = this[1]
        this[0].team.combat.enemy_team_select = true
        this[0].team.combat.update_display()
      }.bind([entity,card])
    
      if ( card.type == "bonus" || card.type == "malus" ){
        content = content.concat([ 
          [' [','#888'],[i,'#0ff'],[']','#888'],[card.type,'#ccc'],
          ['(','#888'],[card.attr,card_colors[card.attr]],[')  ','#888']
        ])
      } else {
        content = content.concat([ 
          [' [','#888'],[i,'#0ff'],[']','#888'],
          [card.type+'  ',card_colors[card.type]],
        ])
      }
    } else {
      if ( card.type == "bonus" || card.type == "malus" ){
        if ( card == entity.combat_action ) {
          content = content.concat([ 
            [' [','#888'],['X','#0ff'],[']','#888'],[card.type,'#ccc'],
            ['(','#888'],[card.attr,card_colors[card.attr]],[')  ','#888']
          ])
        } else {
          content = content.concat([ 
            [card.type,'#ccc'],
            ['(','#888'],[card.attr,card_colors[card.attr]],[')  ','#888']
          ])
        }
      } else {
        content = content.concat([ 
          [card.type+'  ',card_colors[card.type]],
        ])
      }
    }
  } 
  
  let dom = get("#hand")

  while ( dom.children.length ) {
    dom.removeChild(dom.children[0])
  }
  dom.appendChild(col(content))


}

const caste_color = {
  "blade" : "fff",
  "muscle" : "f44",
  "dice" : "4f4",
  "veile" : "44f",
  "jar" : "4ff",
  "spice" : "ff4",
  "stone" : "f4f",
  "card" : "888"
}

const combat_status = "ally_target"

function team_col(team,select){

  let to = team.ia ? "enemy_team" : "player_team"

  let content = [] 

  // # team bar
 
  if ( !team.ia ) {
    content = content.concat([ 
    [n('-',29-team.name.length)+' '+team.name,'#ccc'],[' [','#888'],
    ['u','#0ff'],['][','#888'],['d','#0FF'],[']','#888'],[' -\n','#ccc']
    ])
  } else { 
    content = content.concat([ 
    [n('-',29-team.name.length)+' '+team.name,'#ccc'],[' [','#888'],
    ['p','#0ff'],['][','#888'],['n','#0FF'],[']','#888'],[' -\n','#ccc']
    ])
  }
  
  // # entity card

  for ( var i = 0 ; i < team.entities.length ; i++ ) {
    let entity = team.entities[i]  


    // ## title
    
    if ( select ){
      content = content.concat([ 
        ['- ','#ccc'],['[','#888'],[entity.id,'#0FF'],
        [']','#888'],
        [entity.name+' ' + n('-',17-entity.name.length),'#ccc'],
      ])
    } else {
      content = content.concat([ 
        ['- ','#ccc'],
        [entity.name+' ' + n('-',20-entity.name.length),'#ccc'],
      ])
    }
    
    content = content.concat([ 
    [' PV ','#0F0'],
    [n(' ',3-String(entity.pv).length) + entity.pv+' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.pv).length),'#ccc'],
    [entity.dimmers.pv+' -\n','#ccc']
    ])

  
    // ## portrait

    if ( i == team.display ) {
      if ( !assets[entity.type+'_'+entity.caste] ) {
        content = content.concat([ 
        [assets["missing"],'#'+caste_color[entity.caste]]
        ])
      } else {
        content = content.concat([ 
        [assets[entity.type+'_'+entity.caste],'#'+caste_color[entity.caste]]
        ])
      }


  // ## info bar
    if ( team.ia ){
    content = content.concat([ 
    [n('-',32)+' info -\n '+entity.type+'  '+entity.caste,'#ccc'],
    ['  lvl '+entity.lvl+'\n','#ccc'],
    [' AT ','#f00'],
    ['???','#888'],[' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.at).length)+entity.dimmers.at,'#ccc'],
    [n(' ',6)+'DE ','#08f'],
    ['???','#888'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.de).length)+entity.dimmers.de,'#ccc'],
    ['\n AS ','#ff0'],['???','#888'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.as).length)+entity.dimmers.as,'#ccc'],
    [n(' ',6)+'DS ','#a0a'],
    ['???','#888'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.ds).length)+entity.dimmers.ds,'#ccc'],
    ['\n'+n('-',39)+'\n','#ccc'],
    ])
    }else{
    content = content.concat([ 
    [n('-',32)+' info -\n '+entity.type+'  '+entity.caste,'#ccc'],
    ['  lvl '+entity.lvl+'\n','#ccc'],
    [' AT ','#f00'],
    [n(' ',3-String(entity.at).length) + entity.at + ' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.at).length)+entity.dimmers.at,'#ccc'],
    [n(' ',6)+'DE ','#08f'],
    [n(' ',3-String(entity.de).length) + entity.de,'#ccc'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.de).length)+entity.dimmers.de,'#ccc'],
    ['\n AS ','#ff0'],[n(' ',3-String(entity.as).length)+entity.as,'#ccc'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.as).length)+entity.dimmers.as,'#ccc'],
    [n(' ',6)+'DS ','#a0a'],
    [n(' ',3-String(entity.ds).length) + entity.ds,'#ccc'],
    [' + ','#ccc'],
    [n(' ',4-String(entity.dimmers.ds).length)+entity.dimmers.ds,'#ccc'],
    ['\n'+n('-',39)+'\n','#ccc'],
    ])
    }

    }

  }

  let dom = get('#'+to)

  while ( dom.children.length ) {
    dom.removeChild(dom.children[0])
  }
  dom.appendChild(col(content))
}

// Keyboard input

const assets = {}

var to_load = 0

var keys = {}

document.addEventListener('keydown',function(e){
  if ( keys[e.key] ) keys[e.key](e.key) 
})

