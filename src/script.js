
/* COMMON
   ======
  

   Base functions modification and utils
   ------------------------------------- */

Array.prototype.remove = function(obj){
    let index = this.indexOf(obj);
    if (index == undefined) return;
    let to_concat = this.splice(index);
    let new_arr = this.concat(to_concat.splice(1));
    this.splice(0);
    for ( var i = 0; i < new_arr.length; i++ ){
        this.push(new_arr[i]);
    }
}

function pos(str){
    return str.split(',').map(Number)
}

function get(selector){
    return document.querySelector(selector)
}

function create(tag){
    return document.createElement(tag);
}


/* Variables
   --------- */

var speed = 1000;


/* Graphic interface
   ----------------- */

function event(str,fn){	
    if(fn) fn();
    let event = create('p');	
    let events = get("#events")

    if ( events.children.length ) {
        setTimeout(function(){
	    let event = this.children[0];
	    let history = get("#history");
	    this.removeChild(event);
	    history.appendChild(event);
	}.bind(events),speed);
    }
    
    event.textContent = ' - '+ str;
    events.appendChild(event);
}

function action(clear,str,key,fn){
    let actions = get("#actions");
    
    if (clear){
        while(actions.children.length){
	    actions.removeChild(actions.children[0]);
	}
    }

    let action = create('p');
    action.classList.add("action");

    action.addEventListener("click",fn);

    let shortcut = create("span");
    shortcut.className = "shortcut";
    shortcut.textContent = str[str.indexOf(key)];
    shortcut.id = 'K'+str[str.indexOf(key)];

    let before_shortcut = create("span");
    before_shortcut.textContent = str.slice(0,str.indexOf(key));

    let after_shortcut = create("span");
    after_shortcut.textContent = str.slice(str.indexOf(key)+1);

    action.appendChild(before_shortcut);
    action.appendChild(shortcut);
    action.appendChild(after_shortcut);
    actions.appendChild(action);
}

function map(){


    // Normalize location pos

    var min_pos = [Infinity,Infinity];
    var max_pos = [-Infinity,-Infinity];
    let map_locations = [];
    let map = ""

    for ( var i in locations ){
    	if (pos(i)[0]<min_pos[0]) min_pos[0] = pos(i)[0];
    	if (pos(i)[1]<min_pos[1]) min_pos[1] = pos(i)[1];
    	if (pos(i)[0]>max_pos[0]) max_pos[0] = pos(i)[0];
    	if (pos(i)[1]>max_pos[1]) max_pos[1] = pos(i)[1];
    }

    for ( var i in locations ) {
    	map_locations.push([
	    pos(i)[0]+min_pos[0]*-1,
            pos(i)[1]+min_pos[1]*-1,
            locations[i].relief
	])
    }

    for ( var y = 0 ; y <= max_pos[1]+min_pos[1]*-1 ; y++ ) {
        for ( var x = 0 ; x <= max_pos[0]+min_pos[0]*-1 ; x++ ) {
            var known = false
	    if ( x == local_player_pos[0]+min_pos[0]*-1
              && y == local_player_pos[1]+min_pos[1]*-1 ){
		map += '@';
                known = true;
            } else {	    
            for ( var i = 0 ; i < map_locations.length ; i++ ){
	        if ( x == map_locations[i][0]
                  && y == map_locations[i][1] ) {
		    for ( j in reliefs ) {
	    		let relief = pos(j)
            		if ( map_locations[i][2] >= relief[0]
              		  && map_locations[i][2] <= relief[1] ){
	    		    map += reliefs[j][1];
			}
      		    }
                    known = true;
		}
            }}
	    if (!known) map+=' ';
	}
	map += '\n';
    }

    get("#map_canvas").textContent = map;
}


/* Keyboard interface
   ------------------ */

var key_down = false;

document.addEventListener("keydown",function(e){
    if (key_down) return;
    let key = '#K' + e.key.toUpperCase();
    if (get(key)) {
    	get(key).parentNode.style.backgroundColor = "#440";
        setTimeout(function(){
	    this.click();
	    this.parentNode.style.backgroundColor = "transparent";
	}.bind(get(key)),100);
    }
    if (!key_down) key_down = true;
})

document.addEventListener("keyup",function(e){
    if (key_down) key_down = false;
})


/* GAMEPLAY
   ======== */


/* Settings
   -------- */

const dirs = {
    "0,-1" : "North",
    "-1,0" : "West",
    "1,0" : "East",
    "0,1" : "South"
}

const reliefs = { // name        map
    "-9,0" : 	  [ "sea",      '~'],
    "1,2"  : 	  [ "plains",   '.'],
    "3,5"  : 	  [ "hills",    'n'],
    "6,9"  : 	  [ "mountains",'A']
}

const item_type = {
    "bag" : {
        "carrying_capacity" : 10,
        "materials" : {
            "fiber" : 10
        }
    },
    "food" : {
        "carrying_capacity" : 0,
        "materials" : {}
    }
}

const structure_type = {
    "forest" : {
	"relief"          : [2,6],
	"apparition_rate" : 0.5,
        "item_rate"   : {
	    "wood"    : 0.75,
	    "stone"   : 0.25,
	    "food"    : 0.125
	}
    },
    "cave" : {
	"relief"          : [4,9],
	"apparition_rate" : 0.125,
        "item_rate"   : {
	    "stone"   : 0.75,
	    "iron"    : 0.25,
	    "gold"    : 0.125
	}
    }, 
    "river" : {
	"relief"          : [1,9],
	"apparition_rate" : 0.5,
        "item_rate"   : {
	    "stone"   : 0.75,
	    "food"    : 0.5,
	    "wood"    : 0.25,
	    "gold"    : 0.0125
	}
    },
    "grassland" : {
	"relief"          : [1,4],
	"apparition_rate" : 0.75,
        "item_rate"   : {
	    "stone"   : 0.25,
	    "food"    : 0.125,
	    "wood"    : 0.25
	}
    }
}


/* Time
   ---- */

var day = 0;

function time(duration){
    day += duration;
    get("#day").textContent = day;


    // hunger
    
    for( var i = 0; i < entities.length; i++ ){
        entities[i].pv -= duration
    }
    get("#pv").textContent = entities[0].pv
    if ( entities[0].pv <= 0 ) {
        event("You starved to death");
        action(1,"Restart",'R',function(){
	     document.location.reload(true);
        });
    }

}


/* Locations
   --------- */

const locations = {};

Location = function(pos,relief){

   
    // Setup

    this.pos = pos;
    this.relief = relief;


    // End

    locations[pos] = this;
}


/* Structure
   --------- */

const structures_by_type = {};
const structures_by_pos = {};


Structure = function(pos,type){


    // Setup

    this.pos = pos;
    this.type = type;


    // End

    if (!structures_by_pos[this.pos]) {
	structures_by_pos[this.pos] = [];
    }
    if (!structures_by_type[this.type]) {
	structures_by_type[this.type] = [];
    }

    structures_by_pos[this.pos].push(this);
    structures_by_type[this.type].push(this);
}


/* Entities
   -------- */

const entities_by_pos = {};
const entities = []

Entity = function(pos){


    // Setup

    this.pos = pos;
    this.carrying = ["bag"];
    this.carrying_capacity = 3;
    this.pv = 10;


    // End

    entities.push(this);
    this.travel([0,0]);
}


Entity.prototype.get = function(item,n){
    var capacity = this.carrying_capacity
    for ( var i = 0; i < this.carrying.length; i++ ){
        capacity += item_type[this.carrying[i]].carrying_capacity
    }
    if ( item_type[item].carrying_capacity 
       + capacity
      <= this.carrying_capacity.length ) {
	    this.carrying.push(item);
    } 
}

Entity.prototype.travel = function(dir,verbose){


    // Setup

    let desired_location = 
        locations[[this.pos[0]+dir[0],this.pos[1]+dir[1]]];

    if ( desired_location && desired_location.relief <= 0 ) {
    	event("You need a boat to travel across the seas");
	return;
    }

    var relief = 1;
    var relief_verbose;

    this.pos[0] += dir[0];
    this.pos[1] += dir[1];
    local_player_pos[0] = this.pos[0];
    local_player_pos[1] = this.pos[1];

    if (!entities_by_pos[this.pos]) {
	entities_by_pos[this.pos] = [];
    }


    // Build current location

    if (!locations[this.pos]) { 
	new Location(this.pos,relief);


	// Build structures

	for ( var i in structure_type ) {
	    const type = structure_type[i]
		if ( relief > type.relief[1]
                  || relief < type.relief[0] ) continue;
	        if ( Math.random() < type.apparition_rate ) {
		    new Structure(this.pos,i);
		}
	}

    } else {
    	relief = locations[this.pos].relief;
    }


    // Update status
    
    get("#structures").textContent = '';
    const structures = structures_by_pos[this.pos];

    if ( structures == undefined ) {
        get("#structures").textContent = "barren";
    } else {
        for ( var i = 0; i < structures.length; i++ ){
	    get("#structures").textContent += structures[i].type;
	    if ( i < structures.length-1 ) {
	        get("#structures").textContent += ", ";
	    }
	}
    }
    for ( i in reliefs ) {
        if ( relief >= pos(i)[0]
          && relief <= pos(i)[1] ){
	    get("#location").textContent = reliefs[i][0];
	    relief_verbose = reliefs[i][0];
	}
    }  


    // Build surroundings locations

    for ( var i in dirs ) {
        let dir = pos(i);
        let position = [this.pos[0]+dir[0],this.pos[1]+dir[1]];
	if (!locations[position]) {
	    let dir_relief = relief + (Math.random() > 0.25 ? 
                                           Math.random() > 0.5 ?
                                               relief < 9 ? 1 : 0 
                                           : relief > -9 ? -1 : 0
                                       : 0  );
	    if ( relief > 7 && Math.random() > 0.75 ) relief = 0;
	    if ( relief < -7 && Math.random() > 0.75 ) relief = 6;
	    new Location(position,dir_relief);

	    // Build structures

	    for ( var j in structure_type ) {
	        const type = structure_type[j]
		if ( dir_relief > type.relief[1]
                  || dir_relief < type.relief[0] ) continue;
	        if ( Math.random() < type.rate ) {
		    new Structure(position,j);
		}
	    }
        }
    	for ( j in reliefs ) {
	    let relief = j.split(',').map(Number)
            if ( locations[position].relief >= relief[0]
              && locations[position].relief <= relief[1] ){
		get('#'+dirs[i]).textContent = reliefs[j][0];
	    }
      	}  
        
    }


    // Travel effects

    let duration = relief > 0 ? relief : (relief*-1);

    if (verbose) {
    	event("travel "+dirs[dir]
             +" cross the "+relief_verbose
	     +(duration?(" for "
                 +duration
                 +" day"+(relief>1?'s':''))
             :" in no time!"));
    }
    time(duration);


    // End

    entities_by_pos[this.pos].push(this);
    map();
}


/* PLAYER
   ====== */

const local_player_pos = [0,0]
const local_player = new Entity(local_player_pos);



/* Menus 
   ----- */

function main_menu(){
    action(1,"> Options",'O',function(){
	options_menu();
    });
    action(0,"> Travel",'T',function(){
	travel_menu();
    });
}

const options = [
    ["Status",'S'],
    ["Map",'a'],
    ["History",'H']
]

function options_menu(){
    action(1,"< main Menu",'M',function(){
	main_menu();
    });
    for ( var i = 0 ; i < options.length ; i++ ) {
    	action(0,"- "+options[i][0],options[i][1],function(){
	    let el = get('#'+this);
            if ( el.open ) {
	    	el.style.flex = 0;
            	el.open = false;
	    } else {
	    	el.style.flex = 1;
            	el.open = true;
	    }
    	}.bind(options[i][0]));
    }
}

function travel_menu(){
    action(1,"< main Menu",'M',function(){
	main_menu();
    });
    for ( var i in dirs ) {
	action(0,"- travel "+ dirs[i],dirs[i][0],function(){
    	    local_player.travel(pos(this),1);
	}.bind(i));
    }
    map();
}


/* SETUP
   ===== */

main_menu();



