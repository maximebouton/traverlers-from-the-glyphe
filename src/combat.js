const Combat = function(teams){
  this.teams = teams
  for ( var t = 0 ; t < this.teams.length ; t++ ){
    this.teams[t].combat = this
  }
  this.player_team_select = false
  this.enemy_team_select = false
  this.setup()
}

Combat.prototype.setup = function(){
  for ( var t = 0 ; t < this.teams.length ; t++ ){
    for ( var e = 0 ; e < this.teams[t].entities.length ; e++ ){
      this.teams[t].entities[e].draw() 
    }
  }

  keys['u'] = function(key){
    // if ( this.enemy_team_select ) return
    if ( this.teams[0].display > 0 ){
      this.teams[0].display--
    }
    this.update_display()
  }.bind(this)
   
  keys['d'] = function(key){
    // if ( this.enemy_team_select ) return
    if ( this.teams[0].display 
       < this.teams[0].entities.length-1 ){
      this.teams[0].display++
    }
    this.update_display()
  }.bind(this)
  
  keys['p'] = function(key){
    if ( this.teams[1].display > 0 ){
      this.teams[1].display--
    }
    this.update_display()
  }.bind(this)
  
  keys['n'] = function(key){
    if ( this.teams[1].display 
       < this.teams[1].entities.length-1 ){
      this.teams[1].display++
    }
    this.update_display()
  }.bind(this)
  this.update_display()
}

Combat.prototype.update_display = function(){
  team_col(this.teams[0],this.player_team_select)
  team_col(this.teams[1],this.enemy_team_select)
  hand_row(this.teams[0])
}

/*
Combat.prototype.turn = function(){
  for ( var i = 0 ; i < this.teams.length ; i++ ) {
    if ( this.teams.length == 1 ) return console.log("combat is over")
    for ( var j = 0 ; j < this.teams[i].entities.length ; j ++ ) {
      let entity = this.teams[i].entities[j]
      for ( var k = 0 ; k < entity.wounds.length ; k++ ) {
        entity.hurt()
      }
      // # PV check

      if ( entity.pv 
         + entity.dimmers["pv"] <= 0 ) {
        this.teams[i].entities.remove(entity) 
        if ( !this.teams[i].entities.length ) this.teams.remove(teams[i])
        continue
      } 
 
    }
  }
  for ( var i = 0 ; i < this.teams.length ; i++ ) {
    for ( var j = 0 ; j < this.teams[i].entities.length ; j ++ ) {
      let entity = this.teams[i].entities[j]


      
      // # Action


      // ## Selection

      if ( entity.combat_action == undefined ) {
        if ( entity.team.ia ){
          entity.combat_action = entity.hand.rdm()
        } else {
          choices.push([entity,"combat_action",entity.hand])
        }
        continue
      } 
      if ( entity.combat_target == undefined ) {
        entity.hand.remove(entity.combat_action)
        entity.discard.push(entity.combat_action)
        let type = entity.combat_action.type
        if ( type == "bonus" ){
          if ( entity.team.ia ){
            entity.combat_target = this.teams[i].entities.rdm()
          } else {
            choices.push([entity,"combat_target", this.teams[i].entities])
          }
          continue
        } else {
          if ( entity.team.ia ){
            entity.combat_target = i >= this.teams.length-1   
                                      ? this.teams[0].entities.rdm() 
                                      : this.teams[i+1].entities.rdm()
          } else {
            choices.push([entity,"combat_target", 
                          i >= this.teams.length-1
                             ? this.teams[0].entities 
                             : this.teams[i+1].entities])
          }
          continue
        }
      }
      

      // ## Resolution
      
      if ( entity.combat_action.type == "bonus" 
        || entity.combat_action.type == "malus" ){
        let modifier = entity[entity.combat_action.type](
          entity.combat_action.attr,
          entity.combat_target)
        entity.combat_action = undefined
        entity.combat_target = undefined
        continue
      }
        let dmg = entity[entity.combat_action.type](entity.combat_target)
        entity.combat_action = undefined
        entity.combat_target = undefined
    }
  }
}
*/
