
function setup(){

  // load assets
  
  if ( to_load < assets_to_load.length ) {
    read(assets_to_load[to_load][0],
      function () {
          assets[assets_to_load[to_load][1]] = this.responseText
          to_load++
          setup()
      }
    )
  } else {

  // start game

    const player_0 = new Entity(
      "player_0",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const player_1 = new Entity(
      "player_1",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const player_2 = new Entity(
      "player_2",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const player_team = new Team("player_team",[player_0,player_1,player_2],false)
    
    const enemy_0 =  new Entity(
      "enemy_0",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const enemy_1 =  new Entity(
      "enemy_1",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const enemy_2 =  new Entity(
      "enemy_2",
      list_types().rdm(),
      list_castes().rdm(),
      1
    )
    
    const enemy_team = new Team("enemy_team",[enemy_0,enemy_1,enemy_2],true)
    
    const combat = new Combat([player_team,enemy_team])

  }
}

setup()

