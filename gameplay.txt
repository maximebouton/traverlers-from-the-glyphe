types
-----

         PV  AT  DE  AS  DS
brave    15   2   2   1   1
brutal   10   3   1   2   1
clever    5   2   3   1   2
mystic    5   2   1   3   2
devoted  10   1   2   1   3

"Basic" advantage

brave, clever, mystic  > brutal, mystic
brutal                 > brave, brutal, mystic, devoted

"Special" advantage

brutal > brave, brutal
mystic > brave, brutal, clever, mystic




