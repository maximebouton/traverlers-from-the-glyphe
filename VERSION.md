- [0.7] structure apparition, forest, cave, river, grassland + structure status "Nearby"
- [0.6] remove colored map for the time being
- [0.5](gameplay) need boat to cross sea 
- [0.4](feature) colored map 
- [0.3](feature) map 20200523
- [0.2](fix) crossing the seas takes days 
- [0.1] basic text-mode interface, entities, locations, travel 20200522


## Next

- structures
  + natural
    * lake
    * volcano
  + artificial
    * town
    * castle
    * cemetery
    * tower
    * shrine
    * dungeon
    * ruins
  + building
    * forestry
    * mine
    * mill
    * dam
    * farm
    * forge
    * workshop
  + exploration
    * loot
    * encounter
- structures type
  + relief
  + apparition rate
  + item rate
- materials
  + wood
  + food
  + stone
  + iron
  + gold
  + fiber
- items
  + items type
    * carrying capacity
    * materials
  + boats
	* raft
	* sailboat
	* ship
  + axe
	* wood
	* stone
	* iron
  + basket
  + sword
	* wood
	* stone
	* iron
  + knife
	* stone
	* iron
  + pickaxe
	* stone
	* iron
  + bag
  + crafting
    * ascii art crafting
- entities
  + carrying property
  + carrying_capacity property
  + get function
  + pv ( 10 ? ) propery
  + type property
- entitites_type
  + traits
- time_events
  + hunger
- travel
  + ease travel function from location and structure building
- software
  + separated scripts
  + devlog
- interface
  + browsing
  * shortcuts
  * input
  + page template
  * menu section 
  * view section
  * status section
  * input section
- menus / pages
  + event
    * combat
    * dialog
    * cinematic
  + management
    * world
    * exploration
    * land
    * travel
  + common  
    * options
    * history
    * notice page
    * team
- world
  + events
  + factions
  + lands
  + teams
- multiplayer
  + local
  + online


## Gameplay


### Game Structure

- World
  + Land
    * Structure
- Player / Faction
  + Team
    * Entity


### Main actions

1. Travel
2. Exploration
3. Combat
4. Crafting
5. Management


### Game states

- Management
  + World (map)
    * Show reliefs
    * Show structures
    * Show teams
  + Land
    * Relief status
    * Structures status
    * Building
  + Structure
    * Exploration status
    * Crafting
  + Player / Faction
    * Teams status
    * Team creation / modification
    * Team control
  + Team
    * Travel (Land)
    * Exploration (Structure)
    * Ressources
    * Work (Land, Structure)
  + Entity
    * Equipement
- Combat
  + Types
- Dialog
- Cinematic


### Types

W métal, lumière, bien, mort, innonce, chasteté, pureté, paix, trève, reddition, dialogue, compromis, perte, mort, l'angoisse, le vide, absence (metal, lames)
R Amour / haine honneur, danger, pouvoir, sang, feu, sexe, combat, colère, vigueur (sang, muscles)
G plantes, nature, harmonie, jeunesse, naïveté, energie, liberté, hasard, chance, jalousie, envie, maladie, mort, croissance (sève, cordes? dés? )
B tristesse, mer, eau, voile, discretion, pudeur, mélancolie (eau, voiles? tissus?)
C calme, ciel, silence, sérénité, candeur, paix (ciel, boissons? roue? eau? amphores)
Y gloire, sagesse, sacré, richesse, prospérité, mensonge, hypocrésite, trahison, viellissement, automne, colère, infamie, terre ( > rouge < blanc ) (terre, sable, épices)
M fourberie, tristesse, pénitence, affliction, deuil, saturne, méchansté, initiation, noblesse, jalousie, magie, surnaturel, lune, cristaux (lune, cristaux, pierres)
N privation, renoncement, deuil, rebellion, terreur, inconnu, caché, occulte, illustre, puissant, connaissance, perfection, obligation, devoir, rectitude, constance (charbon ? cartes)

- Lames : attaquant, résistant aux défenseur            10 - 6.5 = 3.5
- Muscles : attaquant, résistant aux attaquants         10 -  11 =  -1
- Dés : attaquant ( RDM ), immunisé aux cartes          7? -  10 = -3?
- Voiles : défenseur, efficace contre les attaquants   8.5 - 5.5 =   3
- Amphores : défenseur, efficace contre les défenseurs 6.5 - 5.5 =   1
- Épices : attaquant, immunisé aux pierres              10 -  11 =  -1
- Pierres : défenseur, point faible des muscles          7 - 5.5 = 1.5
- Cartes : défenseur ( RDM ), point faible des épices    7 -  7? =  0?
   
               +---------------------------------------------------------------------------------------+
               | Defense                                                                               |
               +----------+----------+----------+----------+----------+----------+----------+----------+
               | blade    | muscle   | dice     | veile    | jar      | spice    | stone    | card     |
+---+----------+----------+----------+----------+----------+----------+----------+----------+----------+
| A | blade    | 2        | 1        | 2        | 1        | 1        | 2        | 1        | RDM      | 10 
| t +----------+----------+----------+----------+----------+----------+----------+----------+----------+
| t | muscle   | 2        | 1        | 2        | 1        | 1        | 2        | 1        | RDM      | 10          
| a +----------+----------+----------+----------+----------+----------+----------+----------+----------+
| c | dice     | RDM      | RDM      | RDM      | RDM      | RDM      | RDM      | RDM      | RDM      | ???           
| k +----------+----------+----------+----------+----------+----------+----------+----------+----------+
|   | veile    | 1        | 2        | 2        | 0.5      | 0.5      | 2        | 0.5      | RDM      | 8.5
|   +----------+----------+----------+----------+----------+----------+----------+----------+----------+       
|   | jar      | 0.5      | 1        | 1        | 1        | 1        | 1        | 1        | RDM      | 6.5        
|   +----------+----------+----------+----------+----------+----------+----------+----------+----------+
|   | spice    | 2        | 1        | 2        | 1        | 1        | 2        | 1        | RDM      | 10
|   +----------+----------+----------+----------+----------+----------+----------+----------+----------+
|   | stone    | 0.5      | 4        | 1        | 0.5      | 0.5      | 0        | 0.5      | RDM      | 7
|   +----------+----------+----------+----------+----------+----------+----------+----------+----------+
|   | card     | 0.5      | 1        | 0        | 0.5      | 0.5      | 4        | 0.5      | RDM      | 7
+---+----------+----------+----------+----------+----------+----------+----------+----------+----------+
                 6.5        11         10         5.5        5.5        11         5.5        ???

Attaquants : lames, muscles, épices 2 | voiles 1 | pierres, cartes 0 | Amphores -1
Défenseurs : voiles, amphores, pierres 2 | lames 1 | dés 0 | muscles, épices -1

lames : 3.5
voiles : 3
pierres : 1.5
amphores : 1
cartes : 0?
muscles, épices : -1
dés : -3?


### Classes

- brave
- brute
- clever
- mystic
- devoted

                          weapon      armor       rings       necklace
            +-----------+-----------+-----------+-----------+-----------+
            | PV        | AT        | DE        | AS        | DS        |
+-----------+-----------+-----------+-----------+-----------+-----------+
| brave     | 3         | 2         | 2         | 1         | 1         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| brute     | 2         | 3         | 1         | 2         | 1         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| clever    | 1         | 2         | 3         | 1         | 2         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| mystic    | 1         | 2         | 1         | 3         | 2         |
+-----------+-----------+-----------+-----------+-----------+-----------+
| devoted   | 2         | 1         | 2         | 1         | 3         |
+-----------+-----------+-----------+-----------+-----------+-----------+
    

### Entities

- Lames : attaquant, résistant aux défenseur            10 - 6.5 = 3.5
- Muscles : attaquant, résistant aux attaquants         10 -  11 =  -1
- Dés : attaquant ( RDM ), immunisé aux cartes          7? -  10 = -3?
- Voiles : défenseur, efficace contre les attaquants   8.5 - 5.5 =   3
- Amphores : défenseur, efficace contre les défenseurs 6.5 - 5.5 =   1
- Épices : attaquant, immunisé aux pierres              10 -  11 =  -1
- Pierres : défenseur, point faible des muscles          7 - 5.5 = 1.5
- Cartes : défenseur ( RDM ), point faible des épices    7 -  7? =  0?

blades  brave   10 - 6.5 = 3.5, PV 3  AT 2  RE 2  AS 1  DS 1  
blades  brute   10 - 6.5 = 3.5, PV 2  AT 3  RE 1  AS 2  DS 1
blades  clever  10 - 6.5 = 3.5, PV 1  AT 2  RE 3  AS 1  DS 2
blades  mystic  10 - 6.5 = 3.5, PV 2  AT 3  RE 1  AS 2  DS 1
blades  devoted 
muscles brave
muscles brute
muscles clever
muscles mystic
muscles devoted 
dices   brave
dices   brute
dices   clever
dices   mystic
dices   devoted 


#### 40 rules

 1. sea traveler
 2. plain traveler
 3. hill traveler
 4. mountain traveler
 5. forest salvager
 6. cave salvager
 7. river salvager
 8. lake salvager
 9. volcano salvager
10. town explorer
11. castle explorer
12. cemetery explorer
13. tower explorer
14. shrine explorer
15. dungeon explorer
16. ruins explorer
17. forestry worker
18. mine worker
19. mill worker
20. dam worker
21. farm worker
22. forge worker
23. workshop worker
24. wood tinker
25. food tinker
26. stone tinker
27. iron tinker
28. gold tinker
29. fiber tinker
30. weapon crafter
31. armor crafter
32. ring crafter
33. necklace crafter
34. bag crafter
35. boat crafter
36. axe crafter
37. pickaxe crafter
38. tools crafter
39. builderi
40. beggar





### Control elements

- View
- Shortcut
- Status
- Notice
- List of actions


### Game variables

- World
  + lands
  + players

- Land
  + position  
  + relief
  + structures
  

- Quest

## Notes

suivre text-mode.org sur twitter
https://www.exclusivelygames.com/the-modern-world-of-interactive-fiction/A
Art Typing, Julius Nelson (https://archive.org/details/Artyping/page/n35/mode/2up)
